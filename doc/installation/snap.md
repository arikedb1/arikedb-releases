## Snap installation

Installing [arikedb](https://snapcraft.io/arikedb) and [arikedb-cli](https://snapcraft.io/arikedb-cli) is easy to install
in every linux distribution supporting snaps. Just execute in the terminal with root privileges:

### Installing server

```shell
~$ snap install arikdeb
```

You can check the service is running:

```shell
~$ sudo service snap.arikedb.server status
● snap.arikedb.server.service - Service for snap application arikedb.server
     Loaded: loaded (/etc/systemd/system/snap.arikedb.server.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2023-09-29 17:59:16 CDT; 34s ago
   Main PID: 808178 (python3)
      Tasks: 1 (limit: 18785)
     Memory: 68.7M
        CPU: 6.895s
     CGroup: /system.slice/snap.arikedb.server.service
             └─808178 python3 /snap/arikedb/x1/bin/arikedb

sep 29 17:59:23 workstation arikedb.server[808178]: [INFO] Using config use_auth: False. From 'Default'
...
```

### Installing cli
```shell
~$ snap install arikdeb-cli
```

After install cli you can start querying the arikedb server by typing:

```shell
~$ arikdeb-cli
Connected to Arikedb Server on localhost:6923
▢ 
```

You can connect to servers remotely passing host and port. Type --help for references

```shell
$ arikedb-cli --help
usage: arikedb-cli [-h] [--host HOST] [--port PORT] [--cert CERT] [--exec EXEC [EXEC ...]]

options:
  -h, --help            show this help message and exit
  --host HOST, -H HOST  Server host
  --port PORT, -p PORT  Server port
  --cert CERT, -C CERT  Certificate file
  --exec EXEC [EXEC ...], -e EXEC [EXEC ...]
                        Pass a command

```

> **NOTE**: For now, you should add --edge to get the latest release of arikedb and arikedb-cli since they are in a beta state yet
