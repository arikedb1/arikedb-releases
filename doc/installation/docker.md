## Docker installation

We can run arikedb from its [official docker image](https://hub.docker.com/r/arikedb/arikedb)

```shell
~$ docker pull arikedb/arikedb
```

### Env vars

To modify server configuration while running the arikedb container,
you can use next variables (see [CONFIG command](../cli_references/config.md))
```
ARIKEDB_USE_AUTH=true|false
ARIKEDB_USE_SSL=true|false
ARIKEDB_USE_HTTP_API=true|false
ARIKEDB_HOST=localhost|0.0.0.0|...
ARIKEDB_PORT=8000|...
ARIKEDB_HTTP_HOST=localhost|0.0.0.0|...
ARIKEDB_PORT=8000|...
ARIKEDB_CERT=/path/to/cert_file
ARIKEDB_KEY=/path/to/key_file
LOG_LEVEL=DEBUG|INFO|WARN|ERROR|CRITICAL
```

For example, to run arikedb open to all network interfaces run the container like this:

```shell
~$ docker run ... -e ARIKEDB_HOST=0.0.0.0 arikedb:latest
```

### Persisting the data directory

The arikedb Docker image store all its persistent data in a folder /data inside the container,
to make this folder persistent just add a volume pointing that directory:

```shell
~$ docker run ... -v /local/dir/data:/data arikedb:latest
```

The docker image of arikedb comes with an embedded installed arikedb-cli, so you can execute a shell terminal in your container
and enter arikedb-cli for testing purposes
