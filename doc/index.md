# Arikedb Documentation

Welcome to arikedb documentation.

## Installation

 - [Docker image](installation/docker.md)
 - [Snap Store](installation/snap.md)

## CLI References

### Administration commands

 - [CONFIG command](cli_references/config.md)
 - [LOAD_LICENSE command](cli_references/load_lic.md)
 - [ADD_ROLE command](cli_references/add_role.md)
 - [DEL_ROLE command](cli_references/del_role.md)
 - [ADD_USER command](cli_references/add_user.md)
 - [DEL_USER command](cli_references/del_user.md)
 - [SHOW command](cli_references/del_user.md)
 - [Non-Interactive command execution](cli_references/dont_prompt.md)

### Data manipulation commands

 - [AUTH command](cli_references/auth.md)
 - [ADD_DATABASE command](cli_references/add_database.md)
 - [USE_DATABASE command](cli_references/use_database.md)
 - [DEL_DATABASE command](cli_references/del_database.md)
 - [SET command](cli_references/set.md)
 - [RM command](cli_references/rm.md)
 - [GET command](cli_references/get.md)
 - [PGET command](cli_references/pget.md)
 - [SUBSCRIBE command](cli_references/sub.md)
 - [PSUBSCRIBE command](cli_references/psub.md)
 - [SET_EVENT command](cli_references/set_event.md)
 - [VARIABLES command](cli_references/vars.md)
