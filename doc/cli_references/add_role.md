## ADD_ROLE command

The add_role command allows us to create a new user role in the server. Roles are useful when we want to have users with
different permissions. Every role contains a list of allowed commands and every user with that role will only be able
to execute those commands.

#### Command Schema
```shell
▢ ADD_ROLE <rolename> <alowed-commands>
```

#### Possible commands are:

 - SET
 - RM
 - SET_EVENT
 - GET
 - PGET
 - SUBSCRIBE
 - PSUBSCRIBE
 - ADD_DATABASE
 - DEL_DATABASE
 - ADD_ROLE
 - DEL_ROLE
 - ADD_USER
 - DEL_USER
 - SHOW
 - VARIABLES
 - LOAD_LICENSE
 - CONFIG

#### Examples

###### Adding a new role
```shell
▢ ADD_ROLE getter GET PGET SUBSCRIBE PSUBSCRIBE
Role getter created
```

We can verify the created role by typing SHOW roles (See [SHOW command](show.md))
```shell
▢ SHOW roles
 - admin
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES SET RM SET_EVENT ADD_DATABASE DEL_DATABASE ADD_ROLE DEL_ROLE ADD_USER DEL_USER LOAD_LICENSE CONFIG
 - writer
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES SET RM SET_EVENT ADD_DATABASE DEL_DATABASE
 - reader
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES
 - getter
   > GET PGET SUBSCRIBE PSUBSCRIBE
```

The arikedb server always comes with three default roles.

 - admin
 - writer
 - reader
