## AUTH command

If the arikedb server you are connecting to is configured to use authentication, most of the commands you can run need you to be
authenticated with your user. (See [CONFIG command](config.md)). To do that just use the auth command and provide your credentials

#### Command Schema
```shell
▢ AUTH <username>
```

#### Examples

###### Authenticating as username john_doe which is an admin
```shell
▢ AUTH john_doe
Password: 
Authenticated as john_doe : admin
```
