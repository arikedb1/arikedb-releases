## Possible Events and its parameters
 - **on_set**

  Triggered every time the tag value is set

 - **on_change**

  Triggered every time the tag value is set but only if the new value is
  different from the current one

 - **on_rising_edge**

  Triggered every time the tag value is set but only if the new value is
  greater than the current one

 - **on_falling_edge**

  Triggered every time the tag value is set but only if the new value is
  less than the current one

 - **on_cross_low_threshold**

  Triggered every time the tag value is set but only if the new value is
  less than the specified threshold while the current value is greater than it.
  The threshold is specified by passing the argument THRESHOLD <threshod>

 - **on_cross_high_threshold**

  Triggered every time the tag value is set but only if the new value is
  greater than the specified threshold while the current value is less than it.
  The threshold is specified by passing the argument THRESHOLD <threshod>

 - **on_range_out**

  Triggered every time the tag value is set but only if the new value is
  outside of min and max boundaries while the current value is inside them.
  The boundaries are specified by passing the arguments MIN <min> and MAX <max>

 - **on_range_in**

  Triggered every time the tag value is set but only if the new value is
  inside of min and max boundaries while the current value is outside them.
  The boundaries are specified by passing the arguments MIN <min> and MAX <max>

 - **on_high_pos_rate**

  Triggered every time the tag value is set but only if the rate of changing between
  current and new value is positive and greater than the specified rate threshold. The changing
  rate is calculating by dividing the differences of values and timestamps. The rate
  threshold is specified by passing the argument RATE_THRESHOLD <rate-threshold>

 - **on_high_neg_rate**

  Triggered every time the tag value is set but only if the rate of changing between
  current and new value is negative and greater than the specified rate threshold. The changing
  rate is calculating by dividing the differences of values and timestamps. The rate
  threshold is specified by passing the argument RATE_THRESHOLD <rate-threshold>
