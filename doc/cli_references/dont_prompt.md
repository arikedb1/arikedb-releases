## Executing command without prompt

To run one or multiple commands without entering to interactive prompt cli, use --exec parameter with quoted commands

### Examples

```shell
~$ arikedb-cli --exec "ADD db1" "USE db1" "SET x 23.1 y 12.1 META key1 val1 other 998.0" "PGET *"
Connected to Arikedb Server on localhost:6923
Database db1 created
Using db db1
1696025144608695040 - x: 23.1 key1=val1 other=998.0
1696025144608695040 - y: 12.1 key1=val1 other=998.0
~$
```
