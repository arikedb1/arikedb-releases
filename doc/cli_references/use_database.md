## USE_DATABASE (or USE) command

The use_database command allows us select a database to write and read data

#### Command Schema
```shell
▢ USE_DATABASE|USE <database-name>
```

#### Examples

###### Deleting a database
```shell
▢ USE_DATABASE db1
Using db db1
▢ USE db2
Using db db2
```
