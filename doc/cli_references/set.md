## SET command

The set command allow us to set a new value for one or multiple variables in the current database.
Optionally, for one set command we can add a list of key-value metadata that will be set
for every changed variable. By default, the timestamp is set as current timestamp but a different one can
be set in seconds. When a variable is set by first time, the data type is inferred from the value.

Every variable/meta name or value can be double-quoted to allow using spaces and any special character

#### Command Schema
```shell
SET <variable1> <value1> [<variable2> <value2> ...] [META <key1> <val1> [<key2> <val2> ...]]] [TS <timestamp_sec>]
```

#### Examples

###### Simple variables set
```shell
▢ SET temperature 23.4 humidity 60.0
```

###### Adding metadata
```shell
▢ SET temperature 23.4 humidity 60.0 META "Used instruments" "IA X, HumT56" month October
```
###### Adding a custom timestamp
```shell
▢ SET temperature 23.4 humidity 60.0 META month October TS 1695932676.909943
```

We can use the GET command to check the set values. See [GET command](get.md)
```shell
▢ GET temperature humidity
1695932677909943040 - temperature: 23.4 month=October
1695932677909943040 - humidity: 60.0 month=October
```
