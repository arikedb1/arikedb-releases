## SET_EVENT command

The set_event command allow us to start storing historical data for one or multiple variables based on an event.
The set command of arikedb just store real time in memory data, but you can configure an event to also store that values
in a historical and persistent store. Every time a configured variable is set, if the event condition is triggered, the new value
is stored in the historical storage. Variables can be specified using patterns with same syntax as
pget command (See [PGET command](pget.md)). Depending on the selected event, another parameters should be set.

#### Command Schema
```shell
SET_EVENT <pattern1> [<pattern2> ...] EVENT <event> [THRESHOLD <threshod>] [MIN <min>] [MAX <max>] [RATE_THRESHOLD <rate-threshold>] 
```

See [available events](events.md) for reference

#### Examples

###### Setting different events for some tags
```shell
▢ SET_EVENT var* other[123] EVENT on_set
```
```shell
▢ SET_EVENT var* other?? EVENT on_change
```
```shell
▢ SET_EVENT var* other[ab] EVENT on_rising_edge
```
```shell
▢ SET_EVENT var* EVENT on_falling_edge
```
```shell
▢ SET_EVENT [abc]var EVENT on_cross_low_threshold THRESHOLD 23.1
```
```shell
▢ SET_EVENT [abc]var EVENT on_cross_high_threshold THRESHOLD 80.0
```
```shell
▢ SET_EVENT [abc]var EVENT on_range_out MIN -1.2 MAX 45.22
```
```shell
▢ SET_EVENT [abc]var EVENT on_range_in MIN 1.2 MAX 45.22
```
```shell
▢ SET_EVENT [abc]var EVENT on_high_pos_rate RATE_THRESHOLD 2.2
```
```shell
▢ SET_EVENT [abc]var EVENT on_high_neg_rate RATE_THRESHOLD 1.2
```
