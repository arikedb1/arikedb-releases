## SHOW command

The show command allows us to display a list of current resources. Resources are:
 - roles
 - users
 - databases

#### Command Schema
```shell
▢ SHOW <roles|users|databases>
```

#### Examples

###### Displaying roles
```shell
▢ SHOW roles
 - admin
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES SET RM SET_EVENT ADD_DATABASE DEL_DATABASE ADD_ROLE DEL_ROLE ADD_USER DEL_USER LOAD_LICENSE CONFIG
 - writer
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES SET RM SET_EVENT ADD_DATABASE DEL_DATABASE
 - reader
   > GET PGET SUBSCRIBE PSUBSCRIBE SHOW VARIABLES
```

###### Displaying users
```shell
▢ SHOW users
 - john_doe : admin
```

###### Displaying databases
```shell
▢ SHOW databases
 - db1
 - db2
```
