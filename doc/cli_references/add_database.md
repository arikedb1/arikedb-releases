## ADD_DATABASE (or ADD) command

The add_database command allows us to create a new database.

#### Command Schema
```shell
▢ ADD_DATABASE|ADD <database-name>
```

#### Examples

###### Adding a new database
```shell
▢ ADD_DATABASE db1
Database db1 created
▢ ADD db2
Database db2 created
```

We can verify the created databases by typing SHOW databases (See [SHOW command](show.md))
```shell
▢ SHOW databases
 - db1
 - db1
```
