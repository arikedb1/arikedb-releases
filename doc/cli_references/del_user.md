## DEL_USER command

The del_user command allows us to remove an existing user

#### Command Schema
```shell
▢ DEL_USER <username>
```

#### Examples

###### Removing a user
```shell
▢ DEL_USER john_doe
User john_doe deleted
```
