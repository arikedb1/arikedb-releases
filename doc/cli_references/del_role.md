## DEL_ROLE command

The del_role command allows us to remove an existing role

#### Command Schema
```shell
▢ DEL_ROLE <rolename>
```

#### Examples

###### Removing a role
```shell
▢ DEL_ROLE getter
Role getter deleted. All users with role: getter were also deleted
```
