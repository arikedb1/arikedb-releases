## RM command

The rm command just delete one or multiple variables, including all its historical data in case of some of them have it.
The variables to delete can be specified using patterns with same syntax as pget command (See [PGET command](pget.md))

#### Command Schema
```shell
RM <pattern1> [<pattern2> ...]
```

#### Examples

###### Removing variables by name and patterns
Here we use the vars command to list current existing variables. See [VARIABLES command](variables.md)
```shell
▢ SET var1 12.2 var2 23 var3 "some string" var44 1
▢ VARIABLES *
 - FLOAT: var1
 - INTEGER: var2
 - STRING: var3
 - INTEGER: var44
▢ RM var1 var??
▢ VARIABLES *
 - INTEGER: var2
 - STRING: var3

```
