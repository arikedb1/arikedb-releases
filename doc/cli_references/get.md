## GET command

The get command allow us to read the current real time value of one or multiple variables. Also, if the variables
have a configured event (See [SET_EVENT command](set_event.md)) for historical data saving, using the keys FROM and/or TO
we will read the historical values of those variables. The time of returned timestamps can be expressed in one of next units:
[s, ms, us, ns]

#### Command Schema
```shell
GET <variable1> [<variable2> ...] [EPOCH <s|ms|us|ns|m|u|n>] [FROM <start_timestamp_sec>] [TO <end_timestamp_sec>]
```

#### Examples

###### Getting real time data
```shell
▢ GET temperature humidity
1695933691502798080 - temperature: 21.6 
1695933691502798080 - humidity: 62.8 
```

###### Getting real time data in ms
```shell
▢ GET temperature humidity EPOCH m
1695933691502 - temperature: 21.6 
1695933691502 - humidity: 62.8 
```

###### Getting historical data
```shell
▢ GET temperature humidity EPOCH s FROM 1695922676.221
1695933677.1634862 - temperature: 23.4 
1695933682.3977063 - temperature: 23.6 
1695933685.515594 - temperature: 23.6 
1695933691.502798 - temperature: 21.6 
1695933677.1634862 - humidity: 60.0 
1695933682.3977063 - humidity: 60.2 
1695933685.515594 - humidity: 60.8 
1695933691.502798 - humidity: 62.8 
▢ GET temperature humidity EPOCH s TO 1695945676.221
1695933677.1634862 - temperature: 23.4 
1695933682.3977063 - temperature: 23.6 
1695933685.515594 - temperature: 23.6 
1695933691.502798 - temperature: 21.6 
1695933677.1634862 - humidity: 60.0 
1695933682.3977063 - humidity: 60.2 
1695933685.515594 - humidity: 60.8 
1695933691.502798 - humidity: 62.8 
▢ GET temperature humidity EPOCH s FROM 1695933677.1634862 TO 1695933691.502798
1695933677.1634862 - temperature: 23.4 
1695933682.3977063 - temperature: 23.6 
1695933685.515594 - temperature: 23.6 
1695933677.1634862 - humidity: 60.0 
1695933682.3977063 - humidity: 60.2 
1695933685.515594 - humidity: 60.8
```
