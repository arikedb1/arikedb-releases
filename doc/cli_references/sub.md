## SUBSCRIBE (or SUB) command

The subscribe command allow us to subscribe to one or multiple variables based on an event. Every time one of those variables
is set with a value, and the event condition is triggered we will se the new value.

#### Command Schema
```shell
SUBSCRIBE|SUB <variable1> [<variable2> ...] [EPOCH <s|ms|us|ns|m|u|n>] EVENT <event> [THRESHOLD <threshod>] [MIN <min>] [MAX <max>] [RATE_THRESHOLD <rate-threshold>]
```

See [available events](events.md) for reference

#### Examples

###### Subscribe to real time data
```shell
▢ SUBSCRIBE var1 var2 EVENT on_set
1695998268690345728 - var1: 46 
1695998268690345728 - var2: 87
...
```
```shell
▢ SUBSCRIBE var1 var2 EPOCH s EVENT on_change
1695998410 - var2: 57 
1695998417 - var1: 48
...
```
```shell
▢ SUB var1 var2 EVENT on_rising_edge
...
```
```shell
▢ SUB var1 var2 EVENT on_falling_edge
...
```
```shell
▢ SUB var1 var2 EVENT on_cross_low_threshold THRESHOLD 23.1
...
```
```shell
▢ SUB var1 var2 EVENT on_cross_high_threshold THRESHOLD 80.0
...
```
```shell
▢ SUB var1 var2 EVENT on_range_out MIN -1.2 MAX 45.22
...
```
```shell
▢ SUB var1 var2 EVENT on_range_in MIN 1.2 MAX 45.22
...
```
```shell
▢ SUB var1 var2 EVENT on_high_pos_rate RATE_THRESHOLD 2.2
...
```
```shell
▢ SUB var1 var2 EVENT on_high_neg_rate RATE_THRESHOLD 1.2
...
```
