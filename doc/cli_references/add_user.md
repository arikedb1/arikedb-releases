## ADD_USER command

The add_user command allows us to create a new user in the server.

#### Command Schema
```shell
▢ ADD_USER <rolename> <new-username>
```

#### Examples

###### Adding a new user with role admin
```shell
▢ ADD_USER admin john_doe
Create a password: 
User john_doe: admin created
```

We can verify the created user by typing SHOW users (See [SHOW command](show.md))
```shell
▢ SHOW users
 - john_doe : admin
```
