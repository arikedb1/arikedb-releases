## PGET command

The pget command works exactly like get command (See [GET command](get.md)) with the only difference that here you don't declare
variable names but one or multiple patterns, and only the variables which name matches almost one of those patterns will be retrieved.
Patterns allows any declaration using next special characters:
```
‘*’ – matches everything
‘?’ – matches any single character
‘[seq]’ – matches any character in seq
‘[!seq]’ – matches any character not in seq
```

#### Command Schema
```shell
PGET <pattern1> [<pattern2> ...] [EPOCH <s|ms|us|ns|m|u|n>] [FROM <start_timestamp_sec>] [TO <end_timestamp_sec>]
```

#### Examples

###### Getting real time data
```shell
▢ SET var1 12.2 var2 23 var3 "some string" var44 1
▢ PGET var*
1695934524685588480 - var1: 12.2 
1695934524685588480 - var2: 23 
1695934524685588480 - var3: some string 
1695934524685588480 - var44: 1 
▢ PGET var[12]
1695934524685588480 - var1: 12.2 
1695934524685588480 - var2: 23 
▢ PGET var[!12]
1695934524685588480 - var3: some string 
▢ PGET var?
1695934524685588480 - var1: 12.2 
1695934524685588480 - var2: 23 
1695934524685588480 - var3: some string 
▢ PGET var??
1695934524685588480 - var44: 1 
```
