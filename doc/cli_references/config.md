## CONFIG (or CONF, CFG) command

The config command allows us to change the default configurations of the arikedb server.
Every configuration key can be set by its default value, by an environment value or configured from the cli.

#### Command Schema
```shell
▢ CONFIG|CONF|CFG <SHOW|RESET|SET> [key-value config for SET option]
```

#### Examples

###### To show current server configuration use next command

```shell
▢ CONFIG SHOW
╭───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│                                                                    Arikedb Config                                                                     │
├───────────────┬────────────┬─────────────┬───────────────────────┬────────────┬───────────────────────────────────────────────────────────────────────┤
│      Key      │    Value   │    Source   │        Env Var        │   Default  │                              Description                              │
├───────────────┼────────────┼─────────────┼───────────────────────┼────────────┼───────────────────────────────────────────────────────────────────────┤
│ use_auth      │ False      │ Configured  │ ARIKEDB_USE_AUTH      │ False      │ Define if authentication is needed or not to manipulate arike databa  │
│               │            │             │                       │            │ ses. Changing this key needs a server restart                         │
├───────────────┼────────────┼─────────────┼───────────────────────┼────────────┼───────────────────────────────────────────────────────────────────────┤
│ use_ssl       │ False      │ Default     │ ARIKEDB_USE_SSL       │ False      │ Define if communication with the server will need ssl to connect. If  │
│               │            │             │                       │            │  True, also the keys `cert_file` and `key_file` should be configured  │
│               │            │             │                       │            │ , Changing this key needs a server restart                            │
├───────────────┼────────────┼─────────────┼───────────────────────┼────────────┼───────────────────────────────────────────────────────────────────────┤
│ use_http_api  │ False      │ Default     │ ARIKEDB_USE_HTTP_API  │ False      │ Define if communication with the server throw http API will be enabl  │
│               │            │             │                       │            │ ed. See `http_host` and `http_port`                                   │
├───────────────┼────────────┼─────────────┼───────────────────────┼────────────┼───────────────────────────────────────────────────────────────────────┤
...
```

###### To configure one or multiple keys just use the key name from the previous list

```shell
▢ CFG SET log_level DEBUG port 8000
```

###### To reset all configured keys to its default value use a reset command

```shell
▢ CONF RESET
```


> **NOTE**: Some configurations made need a server restart to be applied
