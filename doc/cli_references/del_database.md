## DEL_DATABASE (or DEL) command

The del_database command allows us to delete a database.

#### Command Schema
```shell
▢ DEL_DATABASE|DEL <database-name>
```

#### Examples

###### Deleting a database
```shell
▢ DEL_DATABASE db1
Database db1 deleted
▢ DEL db2
Database db2 deleted
```
