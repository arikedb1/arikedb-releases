## VARIABLES (or VARS) command

The variables command allow us to list current existing variables in the selected database based on one or more patterns

#### Command Schema
```shell
VARIABLES|VARS <pattern1> [<pattern2> ...]
```

#### Examples

###### Getting variables list
```shell
▢ VARS *
 - FLOAT: var3
 - INTEGER: var2
 - INTEGER: var1
 - INTEGER: var4
```
```shell
▢ VARIABLES var[!12]
 - FLOAT: var3
 - INTEGER: var4
```
```shell
▢ VARIABLES var[!12] var1
 - FLOAT: var3
 - INTEGER: var1
 - INTEGER: var4
```
