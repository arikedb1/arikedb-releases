## LOAD_LICENSE command

Every arikedb server comes with a default free license that allows us to create up to 2 databases and 50 variables on each
one. To load a new license use the load_license command

#### Command Schema
```shell
▢ LOAD_LICENSE <license-file-path>
```

#### Examples

###### Loading a new license
```shell
▢ LOAD_LICENSE /home/.../license.lic
License set. DB Limit: 4 Variable Limit: 500
```
